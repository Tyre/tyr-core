pub trait Evaluable {
    type Output;

    fn eval(self) -> Self::Output;
}

pub trait Specifier<Object> {
    fn specify(self, object: Object) -> Object;
}

pub trait Relation<Object, Specifier> {
    fn relate(self, related: Object) -> Specifier;
}

pub trait Modifier<Word> {
    fn modify(self, word: Word) -> Word;
}

#[derive(Clone, Debug)]
pub struct Entity<S, O> {
    pub properties: Vec<S>,
    pub object: O,
}

impl<S, O> Entity<S, O> {
    pub fn object(object: O) -> Self {
        Self {
            properties: Vec::new(),
            object,
        }
    }
}

impl<S, O> Evaluable for Entity<S, O>
where
    S: Specifier<O>,
{
    type Output = O;

    fn eval(self) -> O {
        self.properties
            .into_iter()
            .rev()
            .fold(self.object, |object, specifier| specifier.specify(object))
    }
}

#[derive(Clone, Debug)]
pub enum PropertyValue<O, S, R, M> {
    Absolute(S),
    Relative {
        relation: R,
        modifiers: Vec<M>,
        entity: Entity<Property<O, S, R, M>, O>,
    },
}

impl<O: Clone, S: Specifier<O>, R: Relation<O, S>, M: Modifier<S> + Modifier<R>> Evaluable
    for PropertyValue<O, S, R, M>
{
    type Output = S;

    fn eval(self) -> S {
        use PropertyValue::*;
        match self {
            Absolute(specifier) => specifier,
            Relative {
                relation,
                modifiers,
                entity,
            } => modifiers
                .into_iter()
                .fold(relation, |relation, modifier| modifier.modify(relation))
                .relate(entity.eval()),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Property<O, S, R, M> {
    pub value: PropertyValue<O, S, R, M>,
    pub modifiers: Vec<M>,
}

impl<O, S, R, M> Property<O, S, R, M> {
    pub fn new(value: PropertyValue<O, S, R, M>) -> Self {
        Self {
            value,
            modifiers: Vec::new(),
        }
    }
}

impl<O: Clone, S: Specifier<O>, R: Relation<O, S>, M: Modifier<S> + Modifier<R>> Evaluable
    for Property<O, S, R, M>
{
    type Output = S;

    fn eval(self) -> S {
        self.modifiers
            .into_iter()
            .fold(self.value.eval(), |specifier, modifier| {
                modifier.modify(specifier)
            })
    }
}

impl<O: Clone, S: Specifier<O>, R: Relation<O, S>, M: Modifier<S> + Modifier<R>> Specifier<O>
    for Property<O, S, R, M>
{
    fn specify(self, object: O) -> O {
        self.eval().specify(object)
    }
}
